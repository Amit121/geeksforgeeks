//
//  HomeVC+Tableview.swift
//  GeeksforGeeksChallenge
//
//  Created by Apple on 27/08/21.
//

import Foundation
import UIKit
import SDWebImage

extension HomeVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homeVMObj.homeObj.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "LargeArticleTVCell") as! LargeArticleTVCell
            cell.selectionStyle = .none
            cell.lbl_Title.text = homeVMObj.homeObj[indexPath.row].title
            if homeVMObj.homeObj[indexPath.row].link != ""{
                cell.img_Profile.sd_setImage(with: URL(string: homeVMObj.homeObj[indexPath.row].link), placeholderImage: #imageLiteral(resourceName: "M3PostAddvert"))
            }
            cell.lbl_Date.text = convertDateToUTCFormate(incomingDateFormatter: .backendFormate, outgoingDateFormatter: .dobFormate, dateString: homeVMObj.homeObj[indexPath.row].pubDate)//
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RegularArticleTVCell") as! RegularArticleTVCell
            cell.selectionStyle = .none
            cell.lbl_Title.text = homeVMObj.homeObj[indexPath.row].title
            if homeVMObj.homeObj[indexPath.row].thumbnail != ""{
                cell.img_Article.sd_setImage(with: URL(string: homeVMObj.homeObj[indexPath.row].thumbnail), placeholderImage: #imageLiteral(resourceName: "M3PostAddvert"))
            }
            cell.lbl_Date.text = convertDateToUTCFormate(incomingDateFormatter: .backendFormate, outgoingDateFormatter: .dobFormate, dateString: homeVMObj.homeObj[indexPath.row].pubDate)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
