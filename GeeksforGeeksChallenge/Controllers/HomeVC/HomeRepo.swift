//
//  HomeRepo.swift
//  GeeksforGeeksChallenge
//
//  Created by Apple on 27/08/21.
//

import Foundation
import Alamofire

class HomeRepo{
    //MARK:-  API hit for Article List.
    func getArticleData(completion: @escaping([HomeModel]?,Bool?,String?) -> Void){
//        showProgressBar()
        let ApiUrl = "https://api.rss2json.com/v1/api.json?rss_url=http://www.abc.net.au/news/feed/51120/rss.xml"
       
        ServiceManager.instance.request(method: .get, URLString: ApiUrl, parameters: nil, encoding: JSONEncoding.default, headers: nil,showLoader: true) { (status, dictionary, error) in
//            self.hideProgressBar()
            if status!{
                print(dictionary as Any)
                let dict = dictionary as? [String:Any]
                let status = dict?["status"] as? String ?? ""
                if status == "ok"{
                    if let items = dict?["items"] as? [[String:Any]]{
                        print(items)
                        var homeData = [HomeModel]()
                        homeData = items.map { HomeModel(data: $0)}
                        completion(homeData, true, "")
                        
//                        if let postList = dict["postList"] as? [[String:Any]]{
//                            print("Post Data: \(postList)")
//                            var postData = [HomeModel]()
//                            postData = postList.map { HomeModel(data: $0)}
//                            completion(postData, true, "")
//                        }
                    }
                }else{
                    completion(nil, false, "")
                }
            }else{
                completion(nil, false, "Something went worng!!!")
            }
        }
    }
}
