//
//  HomeViewModel.swift
//  GeeksforGeeksChallenge
//
//  Created by Apple on 27/08/21.
//

import Foundation

class HomeViewModel: HomeRepo{
    
    var homeObj = [HomeModel]()
    
    //MARK:- Get the data from the server
    func loadHomeData(completion: @escaping([HomeModel]?,Bool?,String?) -> ()){
        self.getArticleData { (article,status,msg ) in
            self.homeObj = article ?? [HomeModel]()
            completion(self.homeObj,status,msg)
        }
    }

}
