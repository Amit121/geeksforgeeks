//
//  HomeModel.swift
//  GeeksforGeeksChallenge
//
//  Created by Apple on 27/08/21.
//

import UIKit

class HomeModel {

    var title = ""
    var pubDate = ""
    var thumbnail = ""
    var link = ""
    
    init() {}
    init(data: [String:Any]) {
        self.title = data["title"] as? String ?? ""
        self.pubDate = data["pubDate"] as? String ?? ""
        self.thumbnail = data["thumbnail"] as? String ?? ""
        if let enclosure = data["enclosure"] as? [String:Any]{
            self.link = enclosure["link"] as? String ?? ""
        }
    }
}
