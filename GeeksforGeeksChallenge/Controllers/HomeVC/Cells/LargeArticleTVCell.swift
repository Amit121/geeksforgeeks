//
//  LargeArticleTVCell.swift
//  GeeksforGeeksChallenge
//
//  Created by Apple on 27/08/21.
//

import UIKit

class LargeArticleTVCell: UITableViewCell {
    
    @IBOutlet weak var view_Container: UIView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var img_Profile: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        view_Container.layer.cornerRadius = 16
        roundUpperBothCorner(cornerRadius: 16)
    }
    func roundUpperBothCorner(cornerRadius : Double){
        img_Profile.layer.cornerRadius = CGFloat(cornerRadius)
        img_Profile.clipsToBounds = true
        img_Profile.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }

}
