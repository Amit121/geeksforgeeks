//
//  RegularArticleTVCell.swift
//  GeeksforGeeksChallenge
//
//  Created by Apple on 27/08/21.
//

import UIKit

class RegularArticleTVCell: UITableViewCell {

    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var view_Container: UIView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var img_Article: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        view_Container.layer.cornerRadius = 16
        roundCorner(cornerRadius: 16)
    }
    func roundCorner(cornerRadius : Double){
        img_Article.layer.cornerRadius = CGFloat(cornerRadius)
        img_Article.clipsToBounds = true
        img_Article.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
    }

}
