//
//  HomeVC.swift
//  GeeksforGeeksChallenge
//
//  Created by Apple on 27/08/21.
//

import UIKit

class HomeVC: UIViewController {
    
    @IBOutlet weak var tableview_HomeArticle: UITableView!
    var homeVMObj = HomeViewModel()
    var refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        loadData()
    }
    //MARK:- Method for SetupUI
    func setUpUI(){
        tableview_HomeArticle.delegate = self
        tableview_HomeArticle.dataSource = self
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        tableview_HomeArticle.addSubview(refreshControl)
    }
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        print("Pull to refreash")
        homeVMObj.homeObj.removeAll()
        loadData()
    }
    //MARK:- Method to Load Post Data
    func loadData(){
        homeVMObj.loadHomeData { (data, status, msg) in
            if !status!{
                self.refreshControl.endRefreshing()
                print("Unsucess",msg ?? "")
            }else{
                print(data?.count ?? 0)
                self.refreshControl.endRefreshing()
                self.tableview_HomeArticle.reloadData()
            }
        }
        
    }
    

}
