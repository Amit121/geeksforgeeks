//
//  DisplayBanner.swift
//  Voowx
//
//  Created by Apple on 24/08/21.
//

import Foundation
import NotificationBannerSwift

class DisplayBanner {
    static let shared = DisplayBanner()
    
    class func successBanner(message: String) {
        NotificationBannerQueue.default.removeAll()
        let banner = GrowingNotificationBanner(title: "Success", subtitle: message, leftView: UIImageView(image: UIImage(named: "M2_Success")), style: .success)
        banner.duration = 0.8
        banner.show()
    }
    
    class func waringBanner(message: String) {
        NotificationBannerQueue.default.removeAll()
        let banner = GrowingNotificationBanner(title: "Failure", subtitle: message, leftView: UIImageView(image: UIImage(named: "M2_Error")), style: .warning)
        banner.duration = 0.8
        banner.show()
    }
    
    class func failureBanner(message: String) {
        NotificationBannerQueue.default.removeAll()
        let banner = GrowingNotificationBanner(title: "Failure", subtitle: message, leftView: UIImageView(image: UIImage(named: "M2_Error")), style: .danger)
        banner.duration = 0.8
        banner.show()
    }
    
    class func infoBanner(message: String) {
        NotificationBannerQueue.default.removeAll()
        let banner = GrowingNotificationBanner(title: "Info", subtitle: message, leftView: UIImageView(image: UIImage(named: "M2_Info")), style: .info)
        banner.duration = 1.0
        banner.show()
    }
}
