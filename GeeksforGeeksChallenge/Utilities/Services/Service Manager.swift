//
//  Service Manager.swift
//  Weeenk
//
//  Created by Shivam on 25/11/19.
//  Copyright © 2019 RipenApps Techonolgy. All rights reserved.
//

import Foundation
import Alamofire
import KRProgressHUD

class ServiceManager: ProgressBar
{
    var reachability : NetworkReachabilityManager?
    static let instance = ServiceManager()
    var alertShowing = false
    
    func request(method: HTTPMethod, URLString: String, parameters: [String : AnyObject]?, encoding: ParameterEncoding, headers: HTTPHeaders?,showLoader: Bool = true ,completionHandler: @escaping (_ success:Bool?,Any?, NSError?) -> ())
    {
        if showLoader {
            self.showProgressBar()
        }

        if ReachabilityOwn.isConnectedToNetwork() == true
        {
//            let kApiURL = baseApiUrl + URLString;
            let kApiURL = URLString;
            print(kApiURL)
            DispatchQueue.global(qos: .utility).async {
                AF.request(kApiURL, method: method, parameters: parameters, encoding: encoding, headers: headers).response(completionHandler: { (response) in
                    if showLoader {
                        self.hideProgressBar()
                    }
                    do{
                        if (response.error == nil)
                        {
                            print(response.data ?? "")
                            let jsonResult = try JSONSerialization.jsonObject(with: response.data!, options: [])
                            DispatchQueue.main.async {
                                completionHandler(true, jsonResult, nil)
                            }
                        }
                        else
                        {
                            print(response.error!)
                            DispatchQueue.main.async {
                                completionHandler(false, nil, response.error! as NSError)
                            }
                        }
                    }
                    catch let error as NSError
                    {
                        print(error)
                        DispatchQueue.main.async {
                            completionHandler(false, nil, error)
                        }
                    }
                })
            }
        }
        else{
//            if(!self.alertShowing)
//            {
//                DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
//                    self.alertShowing = true
//                    let alert = UIAlertController(title: networkProblemTitle, message: kAlertNoNetworkMessage, preferredStyle: UIAlertController.Style.alert)
//                    alert.addAction(UIAlertAction(title: retryTitle, style: UIAlertAction.Style.default, handler: { (act) in
//                        self.alertShowing = false
//                        completionHandler(false ,nil, NSError(domain:"somedomain", code:9001))
//                    }))
//                    alert.addAction(UIAlertAction(title: CancelTitle, style: UIAlertAction.Style.default, handler: { (act) in
//                        self.alertShowing = false
//                        completionHandler(false ,nil, NSError(domain:"somedomain", code:9002))
//                    }))
//                    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
//                }
//            }
            DisplayBanner.failureBanner(message: "Network Problem")
        }
    }
}

extension UIApplication
{

    class func topViewController(base: UIViewController? = UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController) -> UIViewController?
    {
        if let nav = base as? UINavigationController
        {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController
        {
            if let selected = tab.selectedViewController
            {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController
        {
            return topViewController(base: presented)
        }
        return base
    }
}


//                self.alertShowing = true
//                let alert = UIAlertController(title: "Network Problem", message: kAlertNoNetworkMessage, preferredStyle: UIAlertController.Style.alert)
//                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: { (act) in
//                    self.alertShowing = false
//                }))
//                UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
