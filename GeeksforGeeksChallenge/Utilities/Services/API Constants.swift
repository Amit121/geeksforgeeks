//
//  API Constants.swift
//  Weeenk
//
//  Created by Shivam on 25/11/19.
//  Copyright © 2019 RipenApps Techonolgy. All rights reserved.
//

import Foundation
import UIKit

//MARK:- Developement URL
//let baseApiUrl = "http://3.12.253.202:7575/api/"//http://13.235.176.85/mind_alcove/api/"

//MARK:- Production URL
//let BASEURL = "http://65.1.53.243/api/"

let kAlertNoNetworkMessage = "A network connection is required. Please verify your network settings & try again."

