//
//  ProgressBar.swift
//  MVVM_M1
//
//  Created by Apple on 12/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit
import KRProgressHUD

class ProgressBar: NSObject{
    
    //MARK:- func to show progressbar
    func showProgressBar() {
        KRProgressHUD.show()
        KRProgressHUD.set(activityIndicatorViewColors: [UIColor(red: 100/255, green: 131/255, blue: 100/255, alpha: 1.0), UIColor(red: 100/255, green: 131/255, blue: 100/255, alpha: 1.0)])
    }
    
    //MARK:- func to hide progressbar
    func hideProgressBar() {
        KRProgressHUD.dismiss()
    }
    
    //MARK:- func for Api alert
//    func showAlertApi(Alert msg: String) {
//
//        let alertView = UIAlertController(title: appName, message: msg, preferredStyle: .alert)
//        alertView.addAction(UIAlertAction(title: OkTitle, style: .default, handler: {
//            action in
//        }))
//        UIApplication.topViewController()?.present(alertView, animated: true, completion: nil)
//        //self.present(alertView, animated: true, completion: nil)
//    }
}
