//
//  DateFormetter.swift
//  GeeksforGeeksChallenge
//
//  Created by Apple on 27/08/21.
//

import Foundation
import UIKit

enum DateFormateConverter: String{
    case utcFormate = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" // 2020-09-23T07:31:32.332Z"
    case dobFormate = "MMM d, yyyy h:mm a"
    case backendFormate = "yyyy-MM-dd HH:mm:ss"
}

extension UIViewController {
    // MARK: - Func that convert the date into particular format.
    func convertDateToUTCFormate(incomingDateFormatter: DateFormateConverter,outgoingDateFormatter: DateFormateConverter,dateString: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = incomingDateFormatter.rawValue//"yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let date = dateFormatter.date(from: dateString)// create   date from string
        dateFormatter.dateFormat = outgoingDateFormatter.rawValue//"HH:mm"
        dateFormatter.timeZone = NSTimeZone.local
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }
}
